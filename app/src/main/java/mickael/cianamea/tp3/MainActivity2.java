package mickael.cianamea.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity2 extends AppCompatActivity {

    SportDbHelper sportDbHelper = null;
    SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerView);

        sportDbHelper = new SportDbHelper(this);

        if (sportDbHelper.fetchAllTeams().getCount() == 0) {
            sportDbHelper.populate();
        }


        class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            TextView nom = null;
            TextView division = null;
            TextView event = null;
            ImageView badge = null;
            long id;


            RowHolder(View row) {
                super(row);
                nom = row.findViewById(R.id.nom);
                division = row.findViewById(R.id.division);
                event = row.findViewById(R.id.event);
                badge = row.findViewById(R.id.imageView);
                row.setOnClickListener(this);

            }

            @Override
            public void onClick(View v){
                SQLiteDatabase db = sportDbHelper.getReadableDatabase();
                String id1 = "_id=" + id;
                Team team1 = null;
                Cursor cursor = db.query(sportDbHelper.TABLE_NAME, null, id1, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    team1 = sportDbHelper.cursorToTeam(cursor);
                }
                Intent intent = new Intent(MainActivity2.this, TeamActivity.class);
                intent.putExtra(Team.TAG, team1);
                startActivityForResult(intent, 2);

            }

            void bindModel(String nomID, String divisionID, String eventID, String badgeId) {
                nom.setText(nomID);
                division.setText(divisionID);
                event.setText(eventID);
                //badge.setImageResource(badgeId);
            }
        }

        class IconicAdapter extends RecyclerView.Adapter<RowHolder> {
            @Override
            public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return(new RowHolder(getLayoutInflater().inflate(R.layout.row, parent,false)));
            }

            @Override
            public void onBindViewHolder(RowHolder holder,int position) {
                Team team = sportDbHelper.getAllTeams().get(position);
                holder.bindModel(team.getName(), team.getLeague(), team.getLastEvent().toString(), team.getTeamBadge());
            }

            @Override
            public int getItemCount() {
                return(sportDbHelper.getAllTeams().size());
            }

        }

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new IconicAdapter());

        final SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) findViewById(R.id.refreshview);



        /*final Cursor cursor = sportDbHelper.fetchAllTeams();
        adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, sportDbHelper.fetchAllTeams(), new String[]{
                sportDbHelper.COLUMN_TEAM_NAME,
                sportDbHelper.COLUMN_LEAGUE_NAME},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);*/

        //ListView listTeam = (ListView) findViewById(R.id.listTeam);
        //listTeam.setAdapter(adapter);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                Toast toast = Toast.makeText(MainActivity2.this, "Mise à jour en cours...", Toast.LENGTH_SHORT);
                toast.show();
                /*cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    cursor.moveToNext();
                }*/
                swipeLayout.setRefreshing(false);
            }
        });


        /*listTeam.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SQLiteDatabase db = sportDbHelper.getReadableDatabase();
                String id1 = "_id=" + id;
                Team team1 = null;
                Cursor cursor = db.query(sportDbHelper.TABLE_NAME, null, id1, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    team1 = sportDbHelper.cursorToTeam(cursor);
                }
                Intent intent = new Intent(MainActivity2.this, TeamActivity.class);
                intent.putExtra(Team.TAG, team1);
                startActivityForResult(intent, 2);
            }
        });*/

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity2.this, NewTeamActivity.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Team team = (Team) data.getParcelableExtra(Team.TAG);
                if (team != null) {
                    sportDbHelper.addTeam(team);
                    adapter.swapCursor(sportDbHelper.fetchAllTeams());
                    Toast.makeText(MainActivity2.this, "Team ajouté", Toast.LENGTH_LONG).show();
                }
            }
        } else if (requestCode == 2) {
            if (resultCode == 3) {
                Team team = (Team) data.getParcelableExtra(Team.TAG);
                if (team != null) {
                    sportDbHelper.updateTeam(team);
                    adapter.swapCursor(sportDbHelper.fetchAllTeams());
                    Toast.makeText(MainActivity2.this, "Team mis à jour", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}


